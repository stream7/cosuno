export type Company = {
  id: number;
  name: string;
  city: string;
  specialties: string[];
  logoUrl: string;
};
