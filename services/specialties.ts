import { matchSorter } from "match-sorter";
import type { Company } from "../model/company";
import companies from "./data.json";

type Specialty = Company["specialties"][number];

// not a real async function for now
export async function fetchSpecialties(): Promise<Specialty[]> {
  const specialties = companies.map((company) => company.specialties).flat();
  return Array.from(new Set(specialties)).sort();
}
