import { matchSorter } from "match-sorter";
import type { Company } from "../model/company";
import companies from "./data.json";

export type Props = {
  searchTerm: string | undefined;
  specialties: string[] | undefined;
};

// not a real async function for now
export async function fetchCompanies({
  searchTerm,
  specialties,
}: Props): Promise<Company[]> {
  let result = companies;

  if (searchTerm) {
    result = matchSorter(result, searchTerm, { keys: ["name"] });
  }

  if (specialties && specialties.length) {
    result = result.filter((company) =>
      specialties.every((specialty) => company.specialties.includes(specialty))
    );
  }

  return result;
}
