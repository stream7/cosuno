export function serializeQuery(
  query: Record<string, string | string[] | undefined>
): string {
  return Object.entries(query).reduce((acc, [key, value]) => {
    if (!value) return acc;

    const param = Array.isArray(value)
      ? value
          .map((v) => `${encodeURIComponent(key)}=${encodeURIComponent(v)}`)
          .join("&")
      : `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;

    if (!param) return acc;

    return acc + (acc ? "&" : "") + param;
  }, "");
}
