import type { Company } from "../model/company";
import { serializeQuery } from "../lib/url";

export async function fetchCompanies(
  searchTerm: string,
  specialties: string[]
): Promise<Company[]> {
  let url = "/api/companies";
  let query = serializeQuery({ searchTerm, specialties });

  const finalUrl = `${url}${query ? `?${query}` : ""}`;
  const response = await fetch(finalUrl);

  if (!response.ok)
    throw new Error(
      `Failed to fetch companies. Received status ${response.status} for url ${finalUrl}`
    );

  return response.json();
}

export async function fetchSpecialties(): Promise<
  Company["specialties"][number][]
> {
  let url = "/api/specialties";
  const response = await fetch(url);

  if (!response.ok)
    throw new Error(
      `Failed to fetch companies. Received status ${response.status} for url ${url}`
    );

  return response.json();
}
