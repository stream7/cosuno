export function toggleElementFromArray<T>(array: T[], value: T): T[] {
  array = [...array];
  const index = array.indexOf(value);

  if (index === -1) {
    array.push(value);
  } else {
    array.splice(index, 1);
  }
  return array;
}
