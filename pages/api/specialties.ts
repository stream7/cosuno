import type { NextApiRequest, NextApiResponse } from "next";
import type { Company } from "../../model/company";
import { fetchSpecialties } from "../../services/specialties";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Company["specialties"][number][]>
) {
  res.status(200).json(await fetchSpecialties());
}
