import type { NextApiRequest, NextApiResponse } from "next";
import type { Company } from "../../model/company";
import type { ErrorResponse } from "../../model/responses";
import { fetchCompanies, Props } from "../../services/companies";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Company[] | ErrorResponse>
) {
  const query = validateQuery(req.query);

  switch (query.type) {
    case "invalid": {
      res.status(400).json({ message: query.message });
      return;
    }
    case "valid": {
      const companies = await fetchCompanies(query);
      res.status(200).json(companies);
      return;
    }
  }
}

function validateQuery(
  query: NextApiRequest["query"]
): ({ type: "valid" } & Props) | { type: "invalid"; message: string } {
  const { searchTerm, specialties = [] } = query;

  if (searchTerm && Array.isArray(searchTerm))
    return { type: "invalid", message: "Invalid query params" };

  return {
    type: "valid",
    searchTerm,
    specialties: Array.isArray(specialties) ? specialties : [specialties],
  };
}
