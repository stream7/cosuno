import { ChangeEvent, useState, useCallback } from "react";
import { useQuery } from "react-query";

import { fetchCompanies, fetchSpecialties } from "../../lib/clientApi";
import { useDebounce } from "../../lib/hooks";
import { toggleElementFromArray } from "../../lib/arrays";

export function useSearch() {
  const [searchTerm, setSearchTerm] = useState("");
  const [selectedSpecialties, setSelectedSpecialties] = useState<string[]>([]);
  const debouncedSearchTerm = useDebounce(searchTerm, 200);

  const { data, isLoading, isError } = useQuery(
    ["companies", debouncedSearchTerm, [...selectedSpecialties].sort()],
    () => fetchCompanies(debouncedSearchTerm || "", selectedSpecialties)
  );

  const {
    data: specialties,
    isLoading: isSpecialtiesLoading,
    isError: isSpecialtiesError,
  } = useQuery("specialties", fetchSpecialties);

  const onSpecialtyToggle = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setSelectedSpecialties(
        toggleElementFromArray<string>(selectedSpecialties, event.target.value)
      );
    },
    [setSelectedSpecialties, selectedSpecialties]
  );

  return {
    data,
    isLoading: isLoading || isSpecialtiesLoading,
    isError: isError || isSpecialtiesError,
    searchTerm,
    setSearchTerm,
    specialties: specialties || [],
    selectedSpecialties,
    onSpecialtyToggle,
  };
}
