import type { NextPage, NextPageContext } from "next";
import Head from "next/head";
import { Container, Center, Heading } from "@chakra-ui/react";
import { CompaniesContainer } from "./CompaniesContainer";

export const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>Companies</title>
      </Head>

      <Container maxWidth="5xl">
        <header>
          <Center p="10">
            <Heading as="h1">Find your company</Heading>
          </Center>
        </header>

        <main>
          <Container
            maxWidth="100%"
            display="flex"
            flexDirection="column"
            gap="6"
            marginBottom="20"
          >
            <CompaniesContainer />
          </Container>
        </main>
      </Container>
    </>
  );
};
