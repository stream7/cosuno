import {
  Heading,
  Input,
  Stack,
  InputRightElement,
  InputGroup,
  IconButton,
  CheckboxGroup,
  Checkbox,
} from "@chakra-ui/react";
import { CloseIcon } from "@chakra-ui/icons";
import { CompanyCardSkeleton, CompanyCard } from "../../components/CompanyCard";
import { useSearch } from "./useSearch";

export function CompaniesContainer() {
  const {
    data,
    isLoading,
    isError,
    searchTerm,
    setSearchTerm,
    specialties,
    selectedSpecialties,
    onSpecialtyToggle,
  } = useSearch();

  let content;

  if (isLoading) {
    content = [...Array(10)].map((_, index) => (
      <CompanyCardSkeleton key={index} />
    ));
  } else if (isError) {
    content = <Heading>Something went wrong</Heading>;
  } else if (data) {
    content = (
      <>
        {data.length ? (
          data.map((company) => (
            <CompanyCard key={company.id} company={company} />
          ))
        ) : (
          <Heading size="sm">
            No results found for your search. Please try another search term or
            remove a filter.
          </Heading>
        )}
      </>
    );
  }

  return (
    <>
      <Stack spacing="5">
        <InputGroup>
          <Input
            value={searchTerm}
            placeholder="Search for a company"
            onChange={(event) => setSearchTerm(event.target.value)}
            autoFocus
          />
          {searchTerm && (
            <InputRightElement width="4.5rem">
              <IconButton
                aria-label="Clear"
                size="xs"
                rounded="full"
                onClick={() => setSearchTerm("")}
              >
                <CloseIcon />
              </IconButton>
            </InputRightElement>
          )}
        </InputGroup>
        <CheckboxGroup value={selectedSpecialties}>
          <Stack spacing="5" direction={["column", "row"]}>
            {specialties.map((specialty) => (
              <Checkbox
                key={specialty}
                value={specialty}
                isChecked={selectedSpecialties.includes(specialty)}
                onChange={onSpecialtyToggle}
              >
                {specialty}
              </Checkbox>
            ))}
          </Stack>
        </CheckboxGroup>
      </Stack>

      {content}
    </>
  );
}
