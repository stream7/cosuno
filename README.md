This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, install the npm packages and run the development server:

```bash
npm install
npm run dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

The `pages/api` directory is mapped to `/api/*` paths. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## Companies Application

The current project has two API endpoints.

`GET /api/companies` returns a collection of companies and supports search and filtering by specialty.

`GET /api/specialties` returns all the available specialties.

In the UI app we have implemented a responsive design and placeholders for loading states. We also utilized next.js lazy loaded images so that we load only the visible images and thus provide an even faster experience (check it with a simulated slow network). We've used Chakra UI which provides some handy components like the Input and Checkbox components but also various layout components. It's the first time I'm using Chakra so I might have misused it a bit but it made this exercise more interesting for me :)

For data fetching we used react-query which provides a solid foundation for managing data from a server. It gives us automatic retries when a request fails and client side caching for a configured period. We also debounced the request that gets triggered when the user types on the search input in order to avoid excessive and unnecessary requests.

If I had more time to work on this, besides writing tests for my code I would improve the following:

1. DRY up the shared code between CompanyCard and CompanyCardSkeleton.
1. Implement a skeleton component for the loading state of the specialties filters.
1. Move the form state to the URL search params so that I could share a link with pre-selected filters and search term.
1. Implement pagination.
1. Implement server side rendering so that the first render would be faster.
1. Use React Suspense and ErrorBoundaries for handling loading and error states.
1. Change the requested image dimension based on the client's viewport.
