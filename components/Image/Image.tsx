import { useState } from "react";
import NextImage, { ImageProps } from "next/image";
import {
  Box,
  BoxProps,
  Skeleton,
  SkeletonCircle,
  SkeletonProps,
} from "@chakra-ui/react";

type Props = {
  src: ImageProps["src"];
  alt: string;
  width: SkeletonProps["width"];
  minHeight?: SkeletonProps["height"];
};

const alreadyLoaded: Record<string, boolean | undefined> = {};

export function Image(props: Props) {
  const [isLoaded, setLoaded] = useState(!!alreadyLoaded[props.src.toString()]);

  return (
    <>
      {!isLoaded ? (
        <Skeleton
          width={props.width}
          minHeight={"minHeight" in props ? props.minHeight : undefined}
        />
      ) : null}
      <Box
        width={isLoaded ? props.width : 0}
        height={isLoaded && "rounded" in props ? props.width : undefined}
        minHeight={
          isLoaded && "minHeight" in props ? props.minHeight : undefined
        }
        position="relative"
        visibility={isLoaded ? "visible" : "hidden"}
        overflow="hidden"
      >
        <NextImage
          layout="fill"
          src={props.src}
          alt={props.alt}
          objectFit="cover"
          onLoadingComplete={() => {
            setLoaded(true);
            alreadyLoaded[props.src.toString()] = true;
          }}
        />
      </Box>
    </>
  );
}
