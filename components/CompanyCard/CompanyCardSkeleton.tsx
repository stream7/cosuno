import {
  Flex,
  SkeletonText,
  Skeleton,
  WrapItem,
  Spacer,
  Wrap,
  SkeletonCircle,
} from "@chakra-ui/react";

import { MIN_HEIGH } from "./CompanyCard";

// TODO I think Chakra's Skeleton's isLoaded prop could possibly eliminate this component
// if not, many of its elements could be re-used with EpisodeCard
export function CompanyCardSkeleton() {
  return (
    <Flex
      height={{
        base: 2.5 * MIN_HEIGH,
        md: MIN_HEIGH,
      }}
      as="section"
      direction={{
        base: "column",
        md: "row",
      }}
      boxShadow="md"
      rounded="md"
      overflow="hidden"
    >
      <Skeleton
        height={{
          base: MIN_HEIGH,
          md: "100%",
        }}
        width={{
          base: "100%",
          md: "40%",
        }}
      />
      <Flex
        padding="6"
        width={{
          base: "100%",
          md: "60%",
        }}
        direction={{
          base: "column",
          md: "row",
        }}
      >
        <Flex
          direction="column"
          width={{
            base: "100%",
            md: "40%",
          }}
        >
          <SkeletonText mt="4" noOfLines={2} spacing="4" />
        </Flex>
        <Spacer />

        <Wrap
          justify={{
            base: "center",
            md: "right",
          }}
          width={{
            base: "100%",
            md: "60%",
          }}
          marginTop={{
            base: "10",
            md: "0",
          }}
        >
          {[...Array(3)].map((_, index) => (
            <WrapItem key={index}>
              <SkeletonText mt="4" noOfLines={1} spacing="4" />
            </WrapItem>
          ))}
        </Wrap>
      </Flex>
    </Flex>
  );
}
