import {
  Box,
  Flex,
  Heading,
  Text,
  Tooltip,
  Spacer,
  Wrap,
  WrapItem,
  SkeletonCircle,
  Tag,
} from "@chakra-ui/react";
import { Image } from "../Image";
import type { Company } from "../../model/company";

type Props = {
  company: Company;
};

export const MIN_HEIGH = 200;

export function CompanyCard({ company }: Props) {
  return (
    <Flex
      as="article"
      direction={{
        base: "column",
        md: "row",
      }}
      boxShadow="md"
      rounded="md"
      overflow="hidden"
    >
      <Image
        width={{
          base: "100%",
          md: "40%",
        }}
        minHeight={MIN_HEIGH}
        src={company.logoUrl}
        alt={company.name}
      />

      <Flex
        padding="6"
        width={{
          base: "100%",
          md: "60%",
        }}
        direction={{
          base: "column",
          md: "row",
        }}
      >
        <Flex
          direction="column"
          width={{
            base: "100%",
            md: "40%",
          }}
        >
          <Heading as="h3" size="md" marginBottom="2">
            {company.name}
          </Heading>
          <Text color="gray.400">{company.city}</Text>
        </Flex>
        <Spacer />

        <Wrap
          justify={{
            base: "center",
            md: "right",
          }}
          width={{
            base: "100%",
            md: "60%",
          }}
          marginTop={{
            base: "10",
            md: "0",
          }}
        >
          {company.specialties.map((specialty, index) => (
            <WrapItem key={index}>
              <Tag>{specialty}</Tag>
            </WrapItem>
          ))}
        </Wrap>
      </Flex>
    </Flex>
  );
}
